import * as React from 'react'
import { createRoot } from 'react-dom/client'
import { AppContextProvider, useAppContext } from './AppContext'
import { AuthedApp } from './AuthedApp'
import { UnauthedApp } from './UnauthedApp'

const App = () => {
  debugger
  return (
    <AppContextProvider>
      { useAppContext() ? <AuthedApp /> : <UnauthedApp /> }
    </AppContextProvider>
  )
}

createRoot(document.getElementById('root')!).render(<App />)