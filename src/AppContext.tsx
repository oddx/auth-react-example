import React from "react"

const AppContext = React.createContext(undefined)

export const AppContextProvider = (props) => {
  //check if token exists
  const data = {
    name: "me",
    settings: { isAwesome: true },
    locations: [
      "Tucson, Arizona",
      "Hoh Chi Min, Vietnam",
      "Bangkok, Thailand"
    ]
  }

  const login = { 
    // create cookie 
  }

  const logout = {
    // delete cookie
  }

  return <AppContext.Provider value={{data, login, logout}} {...props} />
}

export const useAppContext = () => React.useContext(AppContext)